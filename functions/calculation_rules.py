"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""
from functions.classes import *



def gewicht(x):
    gewicht = 0
    for c in x.get_component_list(-1):
        gewicht += c.properties["mass [g]"]
    return gewicht

def kosten(x):
    kosten = 0
    for d in x.get_component_list(-1):
        kosten += d.properties["price [Euro]"]
    return kosten

def lieferzeit(x):
    lieferzeit = 0
    for e in x.get_component_list(-1):
        lieferzeit += e.properties["delivery time [days]"]
    return lieferzeit

def leistung(x):
    leistung = 0
    volt = x.properties["input voltage [V]"]
    ampere = x.properties["idle current [mA]"]
    leistung = volt*ampere/1000
    return leistung

def spurbreite(x):
    spurbreite = 0
    spurbreite = x.properties["dimension [studs]"]
    return spurbreite

def test_function():
    print("funktioniert")



if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
